package com.truevo.questions;

import com.sun.javafx.cursor.CursorType;

import java.util.ArrayList;
import java.util.List;

public class Question8 {
    public static void main(String[] args){
        System.out.println("Using the last 10 prime numbers below 10,000, sum the values which have a remainder of 3 when divided by 7.");

        Question8 question8 = new Question8();
        //System.out.println(question8.calculatePrimeNumbers(10000));

        System.out.println("The Sum is: " +question8.calculateValuesAfterModulus(question8.calculatePrimeNumbers(10000)));
    }

    /**
     * This method calculates the prime Number with a given limit
     * @param limit - this is the cutoff number
     */
    public long[] calculatePrimeNumbers(long limit){
        //A natural number greater than 1 that has no positive divisors other than 1 and itself
        ArrayList<Integer> primeNumbers = new ArrayList<Integer>();

        long[] lastTenPrimeNumbers = new long[10];
        int count = 0;
        for(long i=limit-1; i>0; i--){
            if(count<10){
                if(primeNumberIdentifier(i)){
                    //primeNumbers.add(i);
                    lastTenPrimeNumbers[count]=i;
                    count++;
                }
            }
        }

        /*for(int i=0;i<10; i++ ){
            System.out.println(lastTenPrimeNumbers[i]);
        }*/
        return lastTenPrimeNumbers;
    }

    public boolean primeNumberIdentifier(long number){

        int count = 0;

        if(number > 1 ){
            //check for even and odd
            for (long i = 1; i<=number; i++ ){
                if(number%i==0){
                    count ++;
                }
            }
        }

        if(count==2){
            //It means it divides by itself and 1
            return true;
        }
        else
        {
            return false;
        }
    }

    public long calculateValuesAfterModulus(long[] primeNumbers){
        //Sum the values which have a remainder of 3 when divided by 7
        long totalValues = 0;

        for(int i=0; i<10; i++){
            if(primeNumbers[i]%7 == 3){
                //System.out.println(primeNumbers[i]);
                totalValues = totalValues+primeNumbers[i];
            }
        }
        return totalValues;
    }
}
