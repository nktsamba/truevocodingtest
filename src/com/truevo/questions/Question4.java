package com.truevo.questions;

import java.util.ArrayList;
import java.util.List;

public class Question4 {

    public static void main(String[] args) {
        System.out.println("For the number 600881475143, what is the largest prime factor?");

        Question4 question4 = new Question4();
        question4.calculatePrimeFactors(question4.calculateFactors(2310));
        //question4.calculatePrimeFactors(question4.calculateFactors(600881475143L));

    }

    public ArrayList<Long> calculateFactors(long number) {
        ArrayList<Long> factors = new ArrayList<>();
        for (long i = 2; i < number; i++) {
            //Factor - A number that divides into another without a remainder
            //System.out.println(i);
            if (number % i == 0) {
                //System.out.println(i);
                factors.add(Long.valueOf(i));
            }
        }

        System.out.println("Factors: ");
        for (long eachFactor : factors) {
            System.out.print(eachFactor + " ");
        }

        System.out.println();

        return factors;
    }

    public ArrayList<Long> calculatePrimeFactors(List<Long> factors){
        ArrayList<Long> primeFactors = new ArrayList<>();
        for (long eachFactor : factors) {
            //Prime Number - A natural number greater than 1 that has no positive divisors other than 1 and itself
            if(primeNumberIdentifier(eachFactor)){
                primeFactors.add(eachFactor);
            }
        }

        System.out.println();
        System.out.println("Prime Factors: ");
        for (long eachFactor : primeFactors) {
            System.out.print(eachFactor + " ");
        }

        return primeFactors;
    }

   public boolean primeNumberIdentifier(long number){

        int count = 0;

        if(number > 1 ){
            //check for even and odd
            for (int i = 1; i<=number; i++ ){
                if(number%i==0){
                    count ++;
                }
            }
        }

        if(count==2){
            //It means it divides by itself and 1
            return true;
        }
        else
        {
            return false;
        }
    }
}
