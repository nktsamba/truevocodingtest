package com.truevo.questions;

import java.util.ArrayList;

public class Question3 {
    public static void main(String[] args) {
        System.out.println("What is the sum total of the odd (1, 3, 5, ...) numbers within a Fibonacci sequence where the value is no larger than three million?");

        //Create Object for Question 3
        Question3 question3 = new Question3();
        System.out.println("The sum total of the odd numbers within a Fibonacci sequence where the value is no larger than three million is: " +
                question3.countOddNumbers(question3.calculateFibonacci(3000000)));
    }

    public ArrayList<Integer> calculateFibonacci(int maxValue){
        //Fibonacci A sequence of numbers in which each number equals the sum of the two preceding numbers. I.e.: 1 1 2 3 5 8...

        ArrayList<Integer> fibonacci = new ArrayList<>();
        int startSequence = 0;

        do{
            if(fibonacci.size()==0 || (fibonacci.size()==1)){
                fibonacci.add(1);
            }
            else{
                startSequence = fibonacci.get(fibonacci.size()-1)+fibonacci.get(fibonacci.size()-2);
                //Check if Value is now larger than the max-limit
                if(startSequence <= maxValue) {
                    fibonacci.add(startSequence);
                }
            }

        }while(startSequence<maxValue);//No larger than, so we have to include the value

    return  fibonacci;
    }

    public int countOddNumbers(ArrayList<Integer> fibonacciSequence){
        int sumTotalOddNumbers=0;

        for(int eachNumber : fibonacciSequence){
            if(eachNumber % 2 != 0){
                sumTotalOddNumbers++;
            }

            //System.out.print(eachNumber + " ");
        }

        return sumTotalOddNumbers;
    }


}
