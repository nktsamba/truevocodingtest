package com.truevo.questions;

public class Question10 {
    public static void main(String[] args){
        System.out.println("Question 10: The code below compiles correctly, but contains some best practice mistakes and poor standards of coding. " +
                "Please list other mistakes.");
        System.out.println();

        System.out.println("1. The method name 'DoWork' should be in camel case but the first word should be in small letters. It should be like: doWork.");
        System.out.println("2. The parameters A and B should be renamed to describe what they represent. A and B is not Descriptive.");
        System.out.println("3. The variables conn and comm should also be renamed to describe what they represent.");
        System.out.println("4. GetValue(0) uses Magic Numbers i.e. we don't know what 0 is. Possible fix a constant with value 0 which describes what 0 represents.");
    }
}
