package com.truevo.questions;

public class Question1 {

    public static void main(String[] args) {
        System.out.println("This prints out the solution to Question 1 - What is the sum total of multiples below 900 of 2 or 4?");

        //Create object of type Question 1 to use to call the private method
        Question1 question1 = new Question1();

        int multiplesOfTwo = question1.calculateMultiples(900, 2);
        int multiplesOfFour = question1.calculateMultiples(900, 4);

        //Print out the result
        System.out.printf("Multiples of two are %d and Multiples of four are %d ", multiplesOfTwo, multiplesOfFour);
        System.out.println();
        System.out.println("The sum of the multiples is: " + (multiplesOfTwo+multiplesOfFour));

    }

    public int calculateMultiples(int limit, int divisor){
        //Multiple - A number that can be divided by another number without a remainder.

        //Not sure if we count from 0 or 1. For this solution I started at 1.
        int count = 0;
        for(int i=1;i<limit; i++){
            if(i%divisor==0){
                //increment the count
                count ++;
            }
        }
        return count;
    }
}
