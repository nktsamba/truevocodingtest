package com.truevo.questions;

import java.util.ArrayList;

public class Question6 {
    public static void main(String[] args) {
        System.out.println("Using a Fibonacci sequence, what is the second term to contain over two thousand individual digits?");
        System.out.println();

        Question3 question3 = new Question3();
        System.out.println(question3.calculateFibonacci(900000000));

        System.out.println("This question uses the same concept for the Fibonacci Sequence for a very large number. I couldn't solve it and will be stuck on the same place.");

    }
}
