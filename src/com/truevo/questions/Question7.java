package com.truevo.questions;


public class Question7 {

    int[] months = new int[12];
    String[] monthsInWords = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
    String[] days = {"MONDAY", "TUESDAY","WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"};
    int saturdayCount =0;

    public static void main(String[] args) {
        System.out.println("During the twentieth century how many Saturdays fell in the second of the month?");

        Question7 question7 = new Question7();
        //Set up the Months - 0 is January and 11 is December
        question7.months[0] = 31;
        question7.months[1] = 28;
        question7.months[2] = 31;
        question7.months[3] = 30;
        question7.months[4] = 31;
        question7.months[5] = 30;
        question7.months[6] = 31;
        question7.months[7] = 31;
        question7.months[8] = 30;
        question7.months[9] = 31;
        question7.months[10] = 30;
        question7.months[11] = 31;


        question7.calculateDays();

        System.out.println();
        System.out.printf("There were %d Saturdays that fell on the 2nd of the Month from 1901 to 2000.", question7.saturdayCount );
    }

    public void calculateDays(){

        int startingYear = 1900; //Start the year from 1900
        int month = 0; //Start in January
        int moveDays = 0; //Start on a Monday


        //Move through all the years
       for(int years=startingYear; years<2001; years++){
           //Move through all the months
           for(int i=0; i<months.length; i++){
               //Check if its a leap year
               if(years % 4 ==0 ){
                   //System.out.println("It's a LEAP year.");
                   if(years==1900){
                       months[1] = 28;
                   }
                   else{
                       months[1] = 29;
                   }

               }

               //System.out.println("YEAR: "+ years + " MONTH: " + monthsInWords[i]);

               for(int j=1; j<=months[i];j++){
                   if(moveDays == 7){
                       moveDays = 0;
                   }
                   //System.out.printf("%d was a %s \n", j , days[moveDays] );

                   //Calculate the Saturdays in the Twentieth Century
                   if(years!=1900){
                       if(j==2 && days[moveDays]=="SATURDAY"){
                           //System.out.println("Add Count");
                           saturdayCount++;
                       }
                  }
                   moveDays++;
               }

               //System.out.println();
           }

       }

    }

}
