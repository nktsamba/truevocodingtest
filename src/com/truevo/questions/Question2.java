package com.truevo.questions;

public class Question2 {
    private static final String QUESTION_2 = "My solution will be to change from XML to HTTP REST. Therefore we further reduce the size of the payload that needs to be transmitted and make the service faster.";
    public static void main(String[] args){
        System.out.println("You have inherited a secure web service component that handles a large volume payment commands across disconnected environments. This web service has been optimized to use as little XML as possible and each message is smaller than 10 KB.");
        System.out.println();
        System.out.println(QUESTION_2);
    }
}
