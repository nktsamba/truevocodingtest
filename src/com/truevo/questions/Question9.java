package com.truevo.questions;

public class Question9 {

    public static void main(String[] args) {
        System.out.println("Identify and explain your least favourite feature within your native language and how you would rectify it.");
        System.out.println();

        System.out.println("My least favourite in Java is the DateTime Feature when trying to convert from any given time to UTC as well " +
                "as Object Comparison in Tests.");

        System.out.println("I would introduce a new static class that handles the DateTime Conversion so one can call it, pass in their time and then get UTC back." );
        System.out.println("For the Object Comparison in Tests - I would create a new library with a merge of different classes that already do the comparison." +
                " \nThat way, there will be no need to import different packages to do the task. All will be consolidated in one library." );
    }

}
