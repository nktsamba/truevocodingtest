package com.truevo.questions;

import org.junit.Assert;
import org.junit.Test;

public class Question4Test {
    @Test
    public void testCalculatePrimeFactor(){
        Question4 question4 = new Question4();

        question4.calculatePrimeFactors(question4.calculateFactors(2310));
        Assert.assertEquals(5, question4.calculatePrimeFactors(question4.calculateFactors(2310)).size());
        Assert.assertEquals(true, question4.calculatePrimeFactors(question4.calculateFactors(2310)).contains(2L));
        Assert.assertEquals(true, question4.calculatePrimeFactors(question4.calculateFactors(2310)).contains(5L));
        Assert.assertEquals(true, question4.calculatePrimeFactors(question4.calculateFactors(2310)).contains(7L));
        Assert.assertEquals(true, question4.calculatePrimeFactors(question4.calculateFactors(2310)).contains(11L));
    }

    /*@Test
    public void testGetSmallestPrimeFactor(ArrayList<Integer> primeFactorList){
        Question4 question4 = new Question4();
        Assert.assertEquals(2, question4.getSmallestPrimeFactor(primeFactorList));
    }

    @Test
    public void testGetLargestPrimeFactor(ArrayList<Integer> primeFactorList){
        Question4 question4 = new Question4();
        Assert.assertEquals(11, question4.getLargestPrimeFactor(primeFactorList));
    }*/

    @Test
    public void testPrimeNumberFactorIdentifier(){
        Question4 question4 = new Question4();
        Assert.assertEquals(true, question4.primeNumberIdentifier(2));
        Assert.assertEquals(true, question4.primeNumberIdentifier(3));
        Assert.assertEquals(true, question4.primeNumberIdentifier(5));
        Assert.assertEquals(true, question4.primeNumberIdentifier(7));
        Assert.assertEquals(false, question4.primeNumberIdentifier(10));
        Assert.assertEquals(false, question4.primeNumberIdentifier(15));
        Assert.assertEquals(false, question4.primeNumberIdentifier(22));
        Assert.assertEquals(false, question4.primeNumberIdentifier(99));

    }
}