package com.truevo.questions;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class Question1Test {

    @Test
    public void testCalculateMultiples(){
        Question1 question1 = new Question1();
        Assert.assertEquals(0, question1.calculateMultiples (1,2));
        Assert.assertEquals(2, question1.calculateMultiples (5,2));
        Assert.assertEquals(4, question1.calculateMultiples (10,2));
        Assert.assertEquals(0, question1.calculateMultiples (-5,2));
    }

}