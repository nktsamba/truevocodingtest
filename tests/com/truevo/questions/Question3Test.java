package com.truevo.questions;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class Question3Test {
    @Test
    public void testCalculateFibonacci(){
        Question3 question3 = new Question3();
        Assert.assertEquals(11, question3.calculateFibonacci(100).size());
        Assert.assertEquals(14, question3.calculateFibonacci(600).size());
        Assert.assertEquals(30, question3.calculateFibonacci(1000000).size());
        Assert.assertEquals(true, question3.calculateFibonacci(1000000).contains(4181));

    }
    @Test
    public void testGetOddNumbersInFibonacciSequence(){
        Question3 question3 = new Question3();
        Assert.assertEquals(4, question3.countOddNumbers(question3.calculateFibonacci(5)));
        Assert.assertEquals(20, question3.countOddNumbers(question3.calculateFibonacci(1000000)));
        Assert.assertEquals(22, question3.countOddNumbers(question3.calculateFibonacci(3000000)));
    }
}