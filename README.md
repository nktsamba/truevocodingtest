#Truevo Coding Test

This is my solution to the Truevo Coding Test done as part of their hiring process.

**Version 1.0.0**

---
##Installation and Getting Started

- This application was developed in Java in the Intellij IDE.
- It can easily be used in any IDE that supports Java.
- The Java version used is 1.8.0_144.
- The package contains separate Classes for each Question, so you can each Question class
in your preferred IDE which compiles Java.

---
#### To install this project
- Clone the project on your system.
- Extract the files.
- Open the extracted folder in your IDE.
- Right click and Run each class (Each class has a Main() method. )


##Contributors

[Ndafara Keith Tsamba](https://bitbucket.org/nktsamba/)

---
##Licences
© Ndafara Keith Tsamba, [Truevo](https://www.truevo.com/)